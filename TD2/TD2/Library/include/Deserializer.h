#pragma once

#include <vector>

namespace uqac::serialize {
	class Deserializer {

	public:
		Deserializer() = default;
		Deserializer(const char*, int);
		~Deserializer() = default;

		int read(char*);

	private:
		std::vector<char> buffer;
		int position;
	};
}