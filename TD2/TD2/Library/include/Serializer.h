#pragma once
#include <vector>
#include <string>

namespace uqac::serialize {
	enum class PlatformEndianess {
		BigEndian,
		LittleEndian
	};

	constexpr PlatformEndianess DetectEndianess();

	class Serializer {

	public:
		Serializer();
		Serializer(int);
		~Serializer() = default;

		template <class T>
		void serialize(T);

		std::string stringify(std::string);

		template <typename T>
		std::enable_if_t<!std::is_convertible<T, std::string>::value, std::string>
		stringify(T&& value) {
			using std::to_string;
			return to_string(std::forward<T>(value));
		}

		std::string getContainer();

		void reset();

	private:
		std::vector<char> container;
		size_t position;
		PlatformEndianess endianess;
	};
}

#include "Serializer.ipp"