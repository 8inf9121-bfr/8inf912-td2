#pragma once
#include <utility>
#include "Serializer.h"
#include "Framework.h"

namespace uqac::compression {
	class Compression {
	private:
		uqac::serialize::Serializer mySerializer;

	public:

		Compression();
		~Compression() = default;

		/*
			Function that compress a char* string and sotre it in mySerializer.
			
			@param toCompress is the string to compress
		*/
		void compressString(const char* toCompress);

		/*
			Compress an int in a smaller type and store it in mySerialize.

			@param toCompress is the int to compress
			@param range is the range of the int to compress
		*/
		void compressIntWithBytePrecision(int toCompress, std::pair<int, int> range);


		/*
			Function thaht compress a float.

			@param floatToCompress is the float to compress
			@param range is the range of the float
			@param int precision is the precision that is required for the float
		*/
		void compressFloatWithBytePrecision(float floatToCompress, std::pair<float, float> range, int precision);

		/*
			Compress a Vector3<float> and serialize it

			@param vectorToCompress is the vector to compress
			@param firstRange is the range of the first float of the vector
			@param secondRange is the range of the second float of the vector
			@param thridRange is the range of the third float of the vector
			@param precision is the precision required for the compression
		*/
		void compressVector3Float(uqac::utils::Vector3<float> vectorToCompress, 
								std::pair<float, float> firstRange, 
								std::pair<float, float> secondRange, 
								std::pair<float, float> thirdRange,
								int precision);

		/*
			Compress a Vector3<int> and serialize it

			@param vectorToCompress is the vector to compress
			@param firstRange is the range of the first int of the vector
			@param secondRange is the range of the second int of the vector
			@param thridRange is the range of the third int of the vector
		*/
		void compressVector3Int(uqac::utils::Vector3<int> vectorToCompress,
			std::pair<int, int> firstRange,
			std::pair<int, int> secondRange,
			std::pair<int, int> thirdRange);

		/*
			Compress a Quaternion and serialize it

			@param quaternionToCompress is the quaternion to compress
			@param precision is the precision required for the compression
		*/
		void compressQuaternion(uqac::utils::Quaternion quaternionToCompress, int precision);

		/*
			Returns all the content of the serializer
		*/
		inline std::string getSerializerContent() { return this->mySerializer.getContainer(); };

	private:
		/*
			Utility from Mark Ransom : https://stackoverflow.com/questions/21191307/minimum-number-of-bits-to-represent-a-given-int
			Get the nb of bit used by an int
		*/
		int Compression::bitsNeeded(int value);

		std::pair<float, unsigned int> maxElement(std::vector<float>);

		/*
		Compress a float with another by storing them in an integer.
		Keep few precision (only three digits after coma)

		@param buffer is the buffer where data will be written
		@param first is the first float to compress
		@param firstRange is the range of the first float
		@param second is the second float to compress
		@param secondRange is the range of the second float
		*/
		void compressFloatWithBitPrecision(unsigned int* buffer,
			float first, std::pair<float, float> firstRange,
			float second, std::pair<float, float> secondRange);

		/*
			Compress two ints as one

			@param buffer is the int* where int will be compressed
			@param first is the first int to compress
			@param firstRange is the range of the first int to compress
			@param second is the second int to compress
			@param secondRange is the range of the second int to compress
		*/
		void compressIntWithBitPrecision(unsigned int* buffer,
			int first, std::pair<int, int> firstRange,
			int second, std::pair<int, int> secondRange);

		/*
			Function that split an int into severals bytes and store it 
			in buffer.

			@param data is the int to cut into bytes
			@param buffer is the buffer where the sliced int will be store
		*/
		void intToString(unsigned int data, char* buffer);
	};
}