#pragma once

#include "Serializer.h"
#include <cstring>
#include <vector>
#include <algorithm>
#include <iostream>

#define STR_MAX_SIZE 256

namespace uqac::serialize {
	/*
	Serializes a char ptr in its vector, store the length as the first character **MINUS 128** (to be able to use the full extent of the byte)
	*/
	template <class T>
	void Serializer::serialize(T myObject) {
		std::string myObjectAsString = stringify(myObject);
		size_t size = myObjectAsString.length();
		char myObjectAsChars[STR_MAX_SIZE];
		strcpy(myObjectAsChars, myObjectAsString.c_str());

		// datas are limited to a size of 255 bytes
		if (size > STR_MAX_SIZE - 1) {
			std::cout << "Datas too big" << std::endl;
			return;
		}

		// If the new data (and it's size indication) can't fit in the container, resize it
		size_t newSize = size + position + 1;
		if (this->container.size() < newSize) {
			this->container.resize(newSize);
		}

		// Insert size of serialized data
		this->container.at(position) = size - 128;
		this->position++;

		// Insert serialized data in container
		for (int i = 0; i < size; i++) {
			this->container.at(position) = myObjectAsChars[i];
			this->position++;
		}

		/*
		auto print = [](const char& n) { std::cout << " " << n; };
		std::for_each(this->container.cbegin(), this->container.cend(), print);
		std::cout << std::endl;
		*/
		//std::cout << "[Serializer] Current size of the serializer: " << this->container.size() << std::endl;
	}
}