#include <iostream>
#include <vector>
#include <cstddef>
#include "Serializer.h"

namespace uqac::serialize {
	constexpr PlatformEndianess DetectEndianess(){
		union {
			uint32_t i;
			char c[4];
		} testEndian = { 0x01020304 };

		return (testEndian.c[0] == 1) ? PlatformEndianess::BigEndian : PlatformEndianess::LittleEndian;
	}

	Serializer::Serializer() {
		this->position = 0;
		this->container = *(new std::vector<char>);
		this->endianess = DetectEndianess();
		//std::cout << "[Serializer] New serializer of size: " << this->container.size() << std::endl;
	}

	Serializer::Serializer(int size) {
		this->position = 0;
		this->container = *(new std::vector<char>(size));
		this->endianess = DetectEndianess();
		//std::cout << "[Serializer] New serializer of size: " << this->container.size() << std::endl;

		if (this->endianess == PlatformEndianess::LittleEndian){
			std::cout << "Little endian it is !" << std::endl;
		}
		else {
			std::cout << "Big endian it is !" << std::endl;
		}
	}

	std::string Serializer::stringify(std::string s) {
		return std::move(s);
	}

	std::string Serializer::getContainer() {
		std::string str(this->container.data(), this->position);//this->container.size());
		return str;
	}

	void Serializer::reset() {
		this->position = 0;
	}
}