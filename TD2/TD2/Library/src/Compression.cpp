#include "../include/Compression.hpp"
#include <iostream>

namespace uqac::compression {

	Compression::Compression() {
		this->mySerializer = uqac::serialize::Serializer();
	}

	void Compression::compressString(const char* toCompress) {
		int i = 0;

		this->mySerializer.serialize<const char*>(toCompress);
	}

	void Compression::compressIntWithBytePrecision(int toCompress, std::pair<int, int> range) {
		// If negative int, translate the range from [min,max] to [min+max, max+max]
		unsigned int positiveInt = 0;
		if (range.first < 0) {
			positiveInt = toCompress + range.second;
		}
		else {
			positiveInt = toCompress;
		}
		
		char buffer[4] = {};
		this->intToString(positiveInt, buffer);
		this->mySerializer.serialize<char*>(buffer);

	}

	void Compression::compressFloatWithBytePrecision(float floatToCompress, std::pair<float, float> range, int precision) {
		int precisionMultiple = pow(10, precision);
		int myFloatIsNowAnInt = precisionMultiple * floatToCompress;
		std::pair<int, int> intRange(range.first * precisionMultiple, range.second * precisionMultiple);

		return compressIntWithBytePrecision(myFloatIsNowAnInt, intRange);
	}

	void Compression::compressVector3Float(uqac::utils::Vector3<float> vectorToCompress,
											std::pair<float, float> firstRange,
											std::pair<float, float> secondRange,
											std::pair<float, float> thirdRange,
											int precision) {
		this->compressFloatWithBytePrecision(vectorToCompress.x, firstRange, precision);
		this->compressFloatWithBytePrecision(vectorToCompress.y, secondRange, precision);
		this->compressFloatWithBytePrecision(vectorToCompress.z, thirdRange, precision);
	}

	void Compression::compressVector3Int(uqac::utils::Vector3<int> vectorToCompress,
										std::pair<int, int> firstRange,
										std::pair<int, int> secondRange,
										std::pair<int, int> thirdRange) {
		this->compressIntWithBytePrecision(vectorToCompress.x, firstRange);
		this->compressIntWithBytePrecision(vectorToCompress.y, secondRange);
		this->compressIntWithBytePrecision(vectorToCompress.z, thirdRange);
	}

	void Compression::compressQuaternion(uqac::utils::Quaternion quaternionToCompress,
										int precision) {
		unsigned int myPow = pow(2, 2);
		unsigned int pow10 = pow(2, 10);
		unsigned int container = 0;

		std::vector<float> myDatas = { quaternionToCompress.eulerAngles.x,  quaternionToCompress.eulerAngles.y, quaternionToCompress.eulerAngles.z, quaternionToCompress.w };

		std::pair<float, unsigned int> myMax = this->maxElement(myDatas);

		container += myMax.second;

		unsigned int containerTemp = 0;
		for (int i = 0; i < myDatas.size(); i++) {
			if (i != myMax.second) {
				containerTemp = (myDatas.at(i) * 1000 + 707) * 0.724;
				container += containerTemp * myPow;
				myPow *= pow10;
			}
		}

		std::pair<int, int> range(-pow(2, 31), (pow(2, 31) - 1));
		int signedContainer = container - pow(2, 31);
		this->compressIntWithBytePrecision(signedContainer, range);
	}

	int Compression::bitsNeeded(int value)
	{
		int bits = 0;
		if (value >= 0x10000)
		{
			bits += 16;
			value >>= 16;
		}
		if (value >= 0x100)
		{
			bits += 8;
			value >>= 8;
		}
		if (value >= 0x10)
		{
			bits += 4;
			value >>= 4;
		}
		if (value >= 0x4)
		{
			bits += 2;
			value >>= 2;
		}
		if (value >= 0x2)
		{
			bits += 1;
			value >>= 1;
		}
		return bits + value;
	}

	void Compression::compressFloatWithBitPrecision(unsigned int* buffer,
		float first, std::pair<float, float> firstRange,
		float second, std::pair<float, float> secondRange) {
		int multiplicator = 100;
		// Mutiply by 1000 to keep three digits of precision for all parameters
		int firstInt = multiplicator * first;
		int secondInt = multiplicator * second;
		std::pair<int, int> firstRangeInt(firstRange.first * multiplicator, firstRange.second * multiplicator);
		std::pair<int, int> secondRangeInt(secondRange.first * multiplicator, secondRange.second * multiplicator);

		return compressIntWithBitPrecision(buffer, firstInt, firstRangeInt, secondInt, secondRangeInt);
	}

	// NOTE : if recurive use, could combien severals int ?
	// TODO : do not handle case where int are to big and can't be compressed in a single int
	void Compression::compressIntWithBitPrecision(unsigned int* buffer,
		int first, std::pair<int, int> firstRange,
		int second, std::pair<int, int> secondRange) {
		// Check first range. Make int positive if range could be negative
		if (firstRange.first < 0) {
			first += firstRange.second;
		}
		// Store first int
		*buffer = first;

		// Check second range. Make int positive if range could be negative
		if (secondRange.first < 0) {
			second += secondRange.second;
		}
		// Compute how many bits second int needs to be stored int the worst case
		int bitsNeeded = 0;
		if (secondRange.first < 0) {
			bitsNeeded = this->bitsNeeded(secondRange.second + -1 * (secondRange.first));
		}
		else {
			bitsNeeded = this->bitsNeeded(secondRange.second);
		}
		//Shift life to the left by the nb of bits needed
		int swiftingBuffer = *buffer;
		swiftingBuffer = swiftingBuffer << bitsNeeded;
		*buffer = swiftingBuffer;
		// Add life to the buffer
		*buffer = *buffer | second;
	}

	void Compression::intToString(unsigned int data, char* buffer) {
		unsigned int remainder = 0;
		unsigned int offset = 0;
		unsigned int power = pow(2, 8);
		while (data > 0) {
			remainder = data - (data / power) * power;
			data = data / power;
			buffer[offset] = remainder -128;
			offset++;
		}
	}

	std::pair<float, unsigned int> Compression::maxElement(std::vector<float> myList) {
		std::pair<float, unsigned int> maxAndIndex(-1, 0);
		for (unsigned int i = 0; i < myList.size(); i++) {
			if (myList.at(i) > maxAndIndex.first) {
				maxAndIndex.first = myList.at(i);
				maxAndIndex.second = i;
			}
		}
		return maxAndIndex;
	}
}