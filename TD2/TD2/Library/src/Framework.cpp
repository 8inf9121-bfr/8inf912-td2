/*
#include "..\include\Framework.h"
#include <math.h>

namespace uqac::utils {
	/* ****************************
	**          Vector3          **
	**************************** */
/*
	Vector3::Vector3() :
		x{ 0.0 }, y{ 0.0 }, z{ 0.0 }
	{ }

	Vector3::Vector3(const float _x, const float _y, const float _z) : 
		x{ _x }, y{ _y }, z{ _z }
	{ }

	Vector3::Vector3(const Vector3* const toCopy) :
		x{ toCopy->x }, y{ toCopy->y }, z{ toCopy->z }
	{ }
*/

	/* ****************************
	**         Quaternion        **
	**************************** */
	/*
	Quaternion::Quaternion() :
		eulerAngles{ Vector3() },
		w{ 0.0 }
	{ }

	Quaternion::Quaternion(const float _x, const float _y, const float _z, const float _w) : 
		eulerAngles{ Vector3(_x, _y, _z) }, 
		w{ _w }
	{ }


	Quaternion::Quaternion(const float angle, const Vector3* const axis)
	{ 
		w = cos(angle / 2.0);
		float angleSin = sin(angle / 2.0);
		x = angleSin * axis.GetX();
		y = angleSin * axis.GetY();
		z = angleSin * axis.GetZ();
	}

	Quaternion::Quaternion(const Quaternion* const toCopy) :
		eulerAngles{ Vector3(toCopy->eulerAngles) },
		w{ toCopy->w }
	{ }
}
*/